//! A crate with types representing concepts in solid-oidc specification.
//!

#![warn(missing_docs)]
#![cfg_attr(docsrs, feature(doc_cfg))]
#![deny(unused_qualifications)]

pub mod id_token;
