//! This crate provides traits and implementations for
//! defining, serving, provisioning solid pods and podsets.
//!

#![warn(missing_docs)]
#![cfg_attr(docsrs, feature(doc_cfg))]
#![deny(unused_qualifications)]

pub mod pod;
pub mod podset;
