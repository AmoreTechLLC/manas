//! I define few typed headers.
//!

pub mod common;

pub mod accept;

pub mod accept_patch;

pub mod accept_post;

pub mod accept_put;

pub mod link;

pub mod location;

pub mod prefer;

pub mod preference_applied;

pub mod slug;

pub mod forwarded;

pub mod x_forwarded_host;

pub mod x_forwarded_proto;

pub mod last_modified;

pub mod wac_allow;

pub mod www_authenticate;
