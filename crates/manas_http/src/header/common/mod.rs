//! I define few common structs to define
//! representing common rules for typed headers.
//!
pub mod media_type;
pub mod qvalue;
