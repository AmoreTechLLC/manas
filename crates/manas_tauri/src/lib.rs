//! This crate provides utilities to integrate manas as a backend in tauri apps.

#![warn(missing_docs)]
#![cfg_attr(docsrs, feature(doc_cfg))]
#![deny(unused_qualifications)]
